package tsc.abzalov.tm.command.sorting;

import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.SORTING_COMMAND;

@SuppressWarnings("unused")
public final class SortingProjectsByEndDateCommand extends AbstractCommand {

    public SortingProjectsByEndDateCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "sort-projects-by-end-date";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Sort projects by end date.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return SORTING_COMMAND;
    }

    @Override
    public void execute() {
        System.out.println("SORT PROJECTS BY END DATE");
        @NotNull val serviceLocator = getServiceLocator();
        @NotNull val projectService = serviceLocator.getProjectService();
        @NotNull val authService = serviceLocator.getAuthService();
        @NotNull val currentUserId = authService.getCurrentUserId();

        val areProjectsExist = projectService.size(currentUserId) != 0;
        if (areProjectsExist) {
            @NotNull val projects = projectService.sortByEndDate(currentUserId);
            var projectIndex = 0;
            for (@NotNull val project : projects) {
                projectIndex = projectService.indexOf(currentUserId, project) + 1;
                System.out.println(projectIndex + ". " + project);
            }

            System.out.println("Projects was sorted by end date\n");
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

}
