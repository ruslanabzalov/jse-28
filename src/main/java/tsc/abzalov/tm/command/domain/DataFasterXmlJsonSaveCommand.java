package tsc.abzalov.tm.command.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.enumeration.CommandType;

import java.io.File;
import java.nio.file.Files;

import static tsc.abzalov.tm.enumeration.CommandType.ADMIN_COMMAND;

@SuppressWarnings("unused")
public final class DataFasterXmlJsonSaveCommand extends AbstractDomainCommand {

    public DataFasterXmlJsonSaveCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "data-fasterxml-json-save";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Save data in JSON format via FasterXML.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return ADMIN_COMMAND;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull val domain = getDomain();

        @NotNull val file = new File(FASTERXML_JSON_FILENAME);
        @NotNull val filePath = file.toPath();
        Files.deleteIfExists(filePath);
        Files.createFile(filePath);

        @NotNull val objectMapper = new ObjectMapper().registerModule(new JavaTimeModule());
        @NotNull val objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
        objectWriter.writeValue(file, domain);

        System.out.println("Data was saved in JSON format via FasterXML.\n");
    }

}
