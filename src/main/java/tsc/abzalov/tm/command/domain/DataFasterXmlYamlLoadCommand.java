package tsc.abzalov.tm.command.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.domain.Domain;
import tsc.abzalov.tm.enumeration.CommandType;

import java.io.File;
import java.io.FileNotFoundException;

import static tsc.abzalov.tm.enumeration.CommandType.ADMIN_COMMAND;

@SuppressWarnings("unused")
public final class DataFasterXmlYamlLoadCommand extends AbstractDomainCommand {

    public DataFasterXmlYamlLoadCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "data-fasterxml-yaml-load";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from YAML format via FasterXML.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return ADMIN_COMMAND;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull val objectMapper = new ObjectMapper(new YAMLFactory())
                .registerModule(new JavaTimeModule());
        @NotNull val file = new File(FASTERXML_YAML_FILENAME);

        if (!file.exists()) {
            @NotNull val fileName = file.getName();
            throw new FileNotFoundException("File " + fileName + " is not exist!");
        }

        @NotNull val domain = objectMapper.readValue(file, Domain.class);
        setDomain(domain);

        System.out.println("Data was loaded from YAML format via FasterXML.\nPlease, re-login.\n");
    }

}
